# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from Heatmap.models import *
from memory_profiler import profile
import time


'''
    Convert Month string to number 
    for example, March 21, 2018 --> 2018-03-21
'''
@profile
def processDate(date_range):
    date_list = str(date_range).split(' - ')

    rst = []
    for each in date_list:
        temp = str(each).split(' ')
        mm = ''
        if temp[0] == 'January':
            mm = '01'
        elif temp[0] == 'February':
            mm = '02'
        elif temp[0] == 'March':
            mm = '03'
        elif temp[0] == 'April':
            mm = '04'
        elif temp[0] == 'May':
            mm = '05'
        elif temp[0] == 'June':
            mm = '06'
        elif temp[0] == 'July':
            mm = '07'
        elif temp[0] == 'August':
            mm = '08'
        elif temp[0] == 'September':
            mm = '09'
        elif temp[0] == 'October':
            mm = '10'
        elif temp[0] == 'November':
            mm = '11'
        elif temp[0] == 'December':
            mm = '12'

        dd = str(temp[1]).replace(',', '')
        if (len(dd) == 1):
            dd = '0' + dd
        yy = str(temp[2])

        rst.append(yy + '-' + mm + '-' + dd)

    return (rst[0], rst[1])


# Create your views here.
# index page
# @profile
def index(req):
    st1 = time.time()
    points = list()

    # fetch data from database
    rst = DataTB.objects.values('Latitude', 'Longitude', 'SpeedKPH', 'SpeedLimitKPH', 'Local_Time')
    rst = list(rst)

    # dictionary to get points for graph
    graph_points = dict()

    for each in rst:

        # if Speed value is NONE, go to next
        if each['SpeedKPH'] == None or each['SpeedLimitKPH'] == None:
            continue

        t = each['Local_Time']
        y = t[:t.index('-')]

        # iterating every value and counting the number of overspeed and underspeed
        if y in graph_points:
            if each['SpeedKPH'] > each['SpeedLimitKPH']:
                graph_points[y][0] += 1
            elif each['SpeedKPH'] < each['SpeedLimitKPH']:
                graph_points[y][1] += 1
        else:
            graph_points[y] = []
            if each['SpeedKPH'] > each['SpeedLimitKPH']:
                graph_points[y].append(1)
                graph_points[y].append(0)
            elif each['SpeedKPH'] < each['SpeedLimitKPH']:
                graph_points[y].append(0)
                graph_points[y].append(1)

        points.append({'lat': each['Latitude'], 'lon': each['Longitude'], 'wei': '%.1f' % (each['SpeedKPH'] / each['SpeedLimitKPH'])})

    # returning all points as the type of dictionary
    g_points = list()
    for k, v in graph_points.items():
        g_points.append({'y': k, 'item1': v[1], 'item2': v[0]})

    print('\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print('Loading First Screen:', time.time() - st1, '(SEC)')
    print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
    return render(req, 'gmap.html', {'points': points, 'g_points': g_points})


# processing ajax request
@csrf_exempt
def onCriteria(req):
    print(req.POST)
    st1 = time.time()
    onOverSpeed = False
    onCity = False
    onDate = False

    min = ''
    max = ''
    city_name = ''

    points = list()

    # getting post form data
    over_speed = req.POST['overspeed']
    city_otp = req.POST['criteria']
    daterange = req.POST['daterange']

    print(city_otp, daterange)

    # checking if each parameter is valid or not
    if over_speed == 'false':
        onOverSpeed = False
    else:
        onOverSpeed = True

    if city_otp == '00000':
        onCity = False
    else:
        onCity = True
        city_name = CriteriaTB.objects.get(optval=city_otp).name

    if daterange == 'false':
        onDate = False
    else:
        onDate = True

    if onDate:
        (min, max) = processDate(daterange)
    # print(min, max)

    # fetch data from database
    rst = DataTB.objects.values('Latitude', 'Longitude', 'SpeedKPH', 'SpeedLimitKPH', 'City', 'Local_Time')

    for each in list(rst):
        if each['SpeedKPH'] == None or each['SpeedLimitKPH'] == None:
            continue
        if each['City'] == None:
            continue
        if each['Local_Time'] == None:
            continue

        if onOverSpeed:
            if each['SpeedKPH'] <= each['SpeedLimitKPH']:
                continue

        if onCity:
            if city_name != each['City']:
                continue

        if onDate:
            if each['Local_Time'] < min or each['Local_Time'] > max:
                continue

        points.append({'lat': each['Latitude'], 'lon': each['Longitude'], 'wei': 1})

    print('\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print('Request Execution Time:', time.time() - st1, '(SEC)')
    print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
    return JsonResponse({'points': points})