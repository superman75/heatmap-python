from django.db import models

# Create your models here.

'''
    Criteria Table
'''


class CriteriaTB(models.Model):
    optval = models.CharField(max_length=100, null=True)
    name = models.CharField(max_length=100, null=True)



'''
    Sample Data
'''
class DataTB(models.Model):
    Vehicle_id = models.IntegerField(null=True)
    Actual_Time = models.CharField(max_length=60, null=True)
    AlertsEnabled = models.IntegerField(null=True)
    Avl_id = models.IntegerField(null=True)
    City = models.CharField(max_length=60, null=True)
    Compass = models.CharField(max_length=30, null=True)
    ControlledAccess = models.CharField(max_length=30, null=True)
    Country = models.CharField(max_length=80, null=True)
    County = models.CharField(max_length=80, null=True)
    CrumbID = models.CharField(max_length=80, null=True)
    Customer_id = models.IntegerField(null=True)
    DirTravel = models.CharField(max_length=30, null=True)
    Driver_id = models.IntegerField(null=True)
    District = models.IntegerField(null=True)
    ErrorCode = models.IntegerField(null=True)
    FunctionalClass = models.IntegerField(null=True)
    GeneratedIncident = models.IntegerField(null=True)
    Ignition = models.CharField(max_length=30, null=True)
    IsHighway = models.IntegerField(null=True)
    Latitude = models.FloatField(null=True)
    Link_ID = models.IntegerField(null=True)
    Local_Time = models.CharField(max_length=80, null=True)
    Longitude = models.FloatField(null=True)
    MapLimit = models.IntegerField(null=True)
    Odometer = models.FloatField(null=True)
    Postcode = models.CharField(max_length=30, null=True)
    Reported_Driver_id = models.CharField(max_length=40, null=True)
    Reported_Customer_id = models.CharField(max_length=40, null=True)
    Reported_Vehicle_id = models.CharField(max_length=40, null=True)
    RouteType = models.IntegerField(null=True)
    RuleLimit = models.IntegerField(null=True)
    Satellites = models.IntegerField(null=True)
    Save = models.IntegerField(null=True)
    Source = models.CharField(max_length=40, null=True)
    Speed0 = models.FloatField(null=True)
    Speed1 = models.FloatField(null=True)
    SpeedKPH = models.FloatField(null=True)
    SpeedLimit = models.FloatField(null=True)
    SpeedLimitKPH = models.FloatField(null=True)
    SpeedLimitMPH = models.FloatField(null=True)
    SpeedMPH = models.FloatField(null=True)
    SpeedType = models.CharField(max_length=40, null=True)
    SpeedType0 = models.CharField(max_length=20, null=True)
    SpeedType1 = models.CharField(max_length=20, null=True)
    Speeding = models.IntegerField(null=True)
    State = models.CharField(max_length=30, null=True)
    Street = models.CharField(max_length=90, null=True)
    TS_Annotate = models.CharField(max_length=60, null=True)
    TS_Create = models.CharField(max_length=60, null=True)
    Vehicle_Type_id = models.IntegerField(null=True)
    VIN = models.CharField(max_length=30, null=True)
    Vehicle_Alias = models.CharField(max_length=40, null=True)
    Fleets = models.CharField(max_length=200, null=True)


'''
    City Table
'''
class CountryTB(models.Model):
    countryname = models.CharField(max_length=60, null=True)
    countyname = models.CharField(max_length=80, null=True)
    cityname = models.CharField(max_length=80, null=True)
