from django.test import TestCase
from Heatmap.models import *

class VehicleTestCase(TestCase):

    def setUp (self):
        DataTB.objects.create(Latitude="124", Longitude="95", SpeedKPH=15.232, SpeedLimitKPH=8.52, City="FONDA", Local_Time="2015-08-15")
        DataTB.objects.create(Latitude="56", Longitude="90", SpeedKPH=6.232, SpeedLimitKPH=25.32, City="FONDA", Local_Time="2015-08-15")
        DataTB.objects.create(Latitude="87", Longitude="123", SpeedKPH=12.232, SpeedLimitKPH=6.32, City="ROTTERDAM", Local_Time="2015-07-15")
        DataTB.objects.create(Latitude="45", Longitude="67", SpeedKPH=8.45, SpeedLimitKPH=25.32, City="BETHLEHEM", Local_Time="2015-07-15")
        DataTB.objects.create(Latitude="98", Longitude="25", SpeedKPH=35.45, SpeedLimitKPH=25.32, City="BETHLEHEM", Local_Time="2015-07-15")


    def isOverSpeed1 (self):
        points = list()

        # make virtual criteria
        onOverSpeed = True
        onCity = True
        city_name = 'FONDA'
        onDate = True
        (min, max) = ('2015-08-10', '2015-09-10')


        rst = DataTB.objects.values('Latitude', 'Longitude', 'SpeedKPH', 'SpeedLimitKPH', 'City', 'Local_Time')

        for each in list(rst):

            if each['SpeedKPH'] == None or each['SpeedLimitKPH'] == None:
                continue
            if each['City'] == None:
                continue
            if each['Local_Time'] == None:
                continue

            if onOverSpeed:
                if each['SpeedKPH'] <= each['SpeedLimitKPH']:
                    continue

            if onCity:
                if city_name != each['City']:
                    continue

            if onDate:
                if each['Local_Time'] < min or each['Local_Time'] > max:
                    continue

            points.append({'lat': each['Latitude'], 'lon': each['Longitude'], 'wei': 1})

        print("A1")

        ##### Test By #####
        ### python manage.py test Heatmap.unittest.VehicleTestCase.isOverSpeed
        self.assertEqual([{'lat': 124.0, 'lon': 95.0, 'wei': 1}], points)


    def isOverSpeed2 (self):
        points = list()

        # make virtual criteria
        onOverSpeed = True
        onCity = True
        city_name = 'BETHLEHEM'
        onDate = True
        (min, max) = ('2015-07-10', '2015-08-10')


        rst = DataTB.objects.values('Latitude', 'Longitude', 'SpeedKPH', 'SpeedLimitKPH', 'City', 'Local_Time')

        for each in list(rst):

            if each['SpeedKPH'] == None or each['SpeedLimitKPH'] == None:
                continue
            if each['City'] == None:
                continue
            if each['Local_Time'] == None:
                continue

            if onOverSpeed:
                if each['SpeedKPH'] <= each['SpeedLimitKPH']:
                    continue

            if onCity:
                if city_name != each['City']:
                    continue

            if onDate:
                if each['Local_Time'] < min or each['Local_Time'] > max:
                    continue

            points.append({'lat': each['Latitude'], 'lon': each['Longitude'], 'wei': 1})

        print("A2")

        ##### Test By #####
        ### python manage.py test Heatmap.unittest.VehicleTestCase.isOverSpeed
        self.assertEqual([{'lat': 198.0, 'lon': 25.0, 'wei': 1}], points)


    def isOverSpeed3 (self):
        points = list()

        # make virtual criteria
        onOverSpeed = False
        onCity = True
        city_name = 'BETHLEHEM'
        onDate = True
        (min, max) = ('2015-07-10', '2015-08-10')


        rst = DataTB.objects.values('Latitude', 'Longitude', 'SpeedKPH', 'SpeedLimitKPH', 'City', 'Local_Time')

        for each in list(rst):

            if each['SpeedKPH'] == None or each['SpeedLimitKPH'] == None:
                continue
            if each['City'] == None:
                continue
            if each['Local_Time'] == None:
                continue

            if onOverSpeed:
                if each['SpeedKPH'] <= each['SpeedLimitKPH']:
                    continue

            if onCity:
                if city_name != each['City']:
                    continue

            if onDate:
                if each['Local_Time'] < min or each['Local_Time'] > max:
                    continue

            points.append({'lat': each['Latitude'], 'lon': each['Longitude'], 'wei': 1})

        print("A3")

        ##### Test By #####
        ### python manage.py test Heatmap.unittest.VehicleTestCase.isOverSpeed
        self.assertEqual([{'lat': 45.0, 'lon': 67.0, 'wei': 1}, {'lat': 98.0, 'lon': 25.0, 'wei': 1}], points)